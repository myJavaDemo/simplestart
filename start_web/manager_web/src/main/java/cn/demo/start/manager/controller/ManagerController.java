package cn.demo.start.manager.controller;

import cn.demo.start.habernate.UserService;
import cn.demo.start.manager.SimpleStartService;
import cn.demo.start.mybatis.MybatisMutilSource;
import cn.demo.start.mybatis.MybatisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/7/25
 */
@Controller
public class ManagerController
{
    @Autowired(required = false)
    private SimpleStartService simpleStartService;

    @Autowired(required = false)
    private UserService userService;

    @Autowired(required = false)
    private MybatisService mybatisService;

    @Autowired(required = false)
    private MybatisMutilSource mybatisMutilSource;

    @RequestMapping("/home")
    public String shophome(@RequestParam Map<String, Object> map)
    {
        String responseInfo = simpleStartService.getResponseInfo(map);
        Map<String, Object> info = simpleStartService.queryResponseInfo(map);
        System.out.println(responseInfo);
        return "hello";
    }

    @RequestMapping("/user")
    @ResponseBody
    public String userhome(@RequestParam Map<String, Object> map)
    {
        userService.findOne();
       return "OK";
    }

    @RequestMapping("/mybatis")
    @ResponseBody
    public String mybatis(@RequestParam Map<String, Object> map)
    {
        mybatisService.findNameById();
        return "OK";
    }

    @RequestMapping("/mybatisMutil")
    @ResponseBody
    public String mybatisMutil(@RequestParam Map<String, Object> map)
    {
        mybatisMutilSource.findTableInfo();
        return "OK";
    }
}
