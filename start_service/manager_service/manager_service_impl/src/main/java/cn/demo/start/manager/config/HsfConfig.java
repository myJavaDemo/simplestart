package cn.demo.start.manager.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/7/25
 */
@Configuration
@ImportResource("classpath:hsf/hsf-config.xml")
public class HsfConfig
{
}
