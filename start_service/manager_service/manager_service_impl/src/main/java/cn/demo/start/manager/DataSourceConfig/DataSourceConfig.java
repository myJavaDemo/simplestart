//package cn.demo.start.manager.DataSourceConfig;
//
//import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.sql.DataSource;
//
//
///**
// * @author : liyang18
// * @description :
// * @date : 2018/7/25
// */
//@Configuration
//public class DataSourceConfig
//{
//    @Bean(name = "managerDataSource")
//    @ConfigurationProperties(prefix="spring.manager")
//    public DataSource managerDataSource() {
//        return DataSourceBuilder.create().build();
//    }
//}
