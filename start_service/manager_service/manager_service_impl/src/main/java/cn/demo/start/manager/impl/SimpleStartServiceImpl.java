package cn.demo.start.manager.impl;

import cn.demo.start.manager.SimpleStartService;
import cn.demo.start.manager.bean.user.User;
import cn.demo.start.manager.bean.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/7/25
 */
@Service
public class SimpleStartServiceImpl implements SimpleStartService
{
    @Autowired
    private UserRepository userRepository;

    /*@Autowired
    private AreaInfoRepository areaInfoRepository;*/

    @Override
    public String getResponseInfo(Map<String, Object> map)
    {
        return "ok";
    }

    @Override
    public Map<String, Object> queryResponseInfo(Map<String, Object> map)
    {
        Long id = 7L;
        User user = userRepository.findOne(id);
        //AreaInfo areaInfo = areaInfoRepository.findOne("00");
        System.out.println(user.toString());
        //System.out.println(areaInfo.toString());
        return null;
    }
}
