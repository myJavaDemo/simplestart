package cn.demo.start.manager.bean.user;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/7/25
 */
public interface UserRepository extends JpaRepository<User, Long>
{
}
