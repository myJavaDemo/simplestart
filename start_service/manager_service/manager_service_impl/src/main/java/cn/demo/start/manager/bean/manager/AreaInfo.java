//package cn.demo.start.manager.bean.manager;
//
//import javax.persistence.*;
//
///**
// * @author : liyang18
// * @description :
// * @date : 2018/7/25
// */
//@Entity
//@Table(name = "t_area_info")
//public class AreaInfo
//{
//    @Id
//    @GeneratedValue
//    @Column(name = "AREA_CODE")
//    private String areaCode;
//
//    @Column(name = "AREA_NAME")
//    private String areaName;
//
//    @Column(name = "AREA_FRAME")
//    private String areaFrame;
//
//    @Column(name = "PARENT_AREA_CODE")
//    private String parentAreaCode;
//
//    @Column(name = "AREA_LEVEL")
//    private String areaLevel;
//
//    public String getAreaCode()
//    {
//        return areaCode;
//    }
//
//    public void setAreaCode(String areaCode)
//    {
//        this.areaCode = areaCode;
//    }
//
//    public String getAreaName()
//    {
//        return areaName;
//    }
//
//    public void setAreaName(String areaName)
//    {
//        this.areaName = areaName;
//    }
//
//    public String getAreaFrame()
//    {
//        return areaFrame;
//    }
//
//    public void setAreaFrame(String areaFrame)
//    {
//        this.areaFrame = areaFrame;
//    }
//
//    public String getParentAreaCode()
//    {
//        return parentAreaCode;
//    }
//
//    public void setParentAreaCode(String parentAreaCode)
//    {
//        this.parentAreaCode = parentAreaCode;
//    }
//
//    public String getAreaLevel()
//    {
//        return areaLevel;
//    }
//
//    public void setAreaLevel(String areaLevel)
//    {
//        this.areaLevel = areaLevel;
//    }
//
//    @Override
//    public String toString()
//    {
//        return "AreaInfo{" + "areaCode='" + areaCode + '\'' + ", areaName='" + areaName + '\'' + ", areaFrame='" + areaFrame + '\'' + ", parentAreaCode='" + parentAreaCode + '\'' + ", areaLevel='" + areaLevel + '\'' + '}';
//    }
//}
