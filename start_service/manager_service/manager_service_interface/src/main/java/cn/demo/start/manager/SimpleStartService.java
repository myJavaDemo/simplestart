package cn.demo.start.manager;

import java.util.Map;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/7/25
 */
public interface SimpleStartService
{
    String getResponseInfo(Map<String, Object> map);

    Map<String, Object> queryResponseInfo(Map<String, Object> map);
}
