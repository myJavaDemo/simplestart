package com.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/4/22
 */
public class IOServer {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8000);
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Socket socket = serverSocket.accept();

                        new Thread(() -> {
                            byte[] data = new byte[1024];
                            try {
                                InputStream inputStream = socket.getInputStream();
                                while (true){
                                    int len;
                                    while ((len = inputStream.read(data)) != -1){
                                        System.out.println(new String(data, 0, len));
                                    }
                                }
                            }
                            catch (IOException e) {
                                e.printStackTrace();
                            }
                        }).start();
                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
