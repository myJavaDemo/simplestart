package com.proxy.cglib;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/2/22
 */
public class PersonDao {
    public void findStudentInfo(String name){
        System.out.println(name + "查找信息");
    }
}
