package com.proxy.produce;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/4/22
 */
public class FactoryStaticProxy implements ProduceFactory{
    ProduceFactory produceFactory;

    public FactoryStaticProxy(ProduceFactory factory){
        this.produceFactory = factory;
    }

    @Override
    public String creatProduce(String factoryName) {
        System.out.println("start");
        String s = produceFactory.creatProduce(factoryName);
        System.out.println("end");
        return s;
    }

    public static void main(String[] args){
        ProduceFactory produceFactory = new Apple();
        ProduceFactory produce = (ProduceFactory) new FactoryStaticProxy(produceFactory);
        produce.creatProduce("apple");
    }
}
