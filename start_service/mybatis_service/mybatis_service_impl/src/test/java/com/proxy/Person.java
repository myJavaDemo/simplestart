package com.proxy;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/2/21
 */
public interface Person {
    void giveMoney();
}
