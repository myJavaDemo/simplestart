package com.proxy.produce;

import com.proxy.jdk.PersonInvocation;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/4/22
 */
public class FactoryJdkProxy implements InvocationHandler {
    Object obj;

    public FactoryJdkProxy(Object obj) {
        this.obj = obj;
    }

    public Object init() {
        return Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(), this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("start");
        return  method.invoke(obj, args);
    }

    public static void main(String[] args) {
        ProduceFactory produce = new Apple();
        FactoryJdkProxy factoryJdkProxy = new FactoryJdkProxy(produce);
        ProduceFactory factory = (ProduceFactory) factoryJdkProxy.init();
        String s = factory.creatProduce("apple");
        System.out.println(s);
    }
}
