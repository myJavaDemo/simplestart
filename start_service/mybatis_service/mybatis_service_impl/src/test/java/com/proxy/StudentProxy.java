package com.proxy;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/2/21
 */
public class StudentProxy implements Person{
    private Student student;

    public StudentProxy(Person stu){
        if (stu.getClass() == Student.class){
            this.student = (Student) stu;
        }
    }
    @Override
    public void giveMoney() {
        student.giveMoney();
    }
}
