package com.proxy;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/2/21
 */
public class Student implements Person {
    private String name;

    public Student(String name)
    {
        this.name = name;
    }

    @Override
    public void giveMoney() {
        System.out.println(name + "上交50元");
    }
}
