package com.proxy.jdk;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/2/22
 */
public interface IPerson {
    void updateInfo();
}
