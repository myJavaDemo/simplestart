package com.proxy.cglib;

import org.junit.Test;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/2/22
 */
public class TestCglib {
    @Test
    public void testCglib() {
        PersonDao personDao = new PersonDao();
        CglibProxyFactory proxyFactory = new CglibProxyFactory(personDao);
        PersonDao dao = (PersonDao) proxyFactory.getProxyFactory();
        dao.findStudentInfo("me");
    }
}
