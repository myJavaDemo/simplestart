package com.proxy.produce;

import org.mockito.cglib.proxy.Enhancer;
import org.mockito.cglib.proxy.MethodInterceptor;
import org.mockito.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/4/22
 */
public class FactoryCglibroxy implements MethodInterceptor{
    Object object;

    public Object initProxy(Object obj) {
        this.object = obj;
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(object.getClass());
        enhancer.setCallback(this);
        return enhancer.create();
    }

    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        System.out.println("logger");
        return method.invoke(object, args);
    }

    public static void main(String[] args){
        Orange apple = new Orange();
        FactoryCglibroxy factoryCglibroxy = new FactoryCglibroxy();
        Orange proxy = (Orange) factoryCglibroxy.initProxy(apple);
        proxy.creatProduce("apple");
    }
}
