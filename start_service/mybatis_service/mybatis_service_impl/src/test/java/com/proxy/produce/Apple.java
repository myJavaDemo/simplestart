package com.proxy.produce;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/4/22
 */
public class Apple implements ProduceFactory {
    @Override
    public String creatProduce(String factoryName) {
        System.out.println("生产" + factoryName);
        return factoryName;
    }
}
