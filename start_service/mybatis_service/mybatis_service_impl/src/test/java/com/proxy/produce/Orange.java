package com.proxy.produce;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/4/22
 */
public class Orange {
    public String creatProduce(String name) {
        System.out.println("生产" + name);
        return name;
    }
}
