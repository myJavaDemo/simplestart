package com.proxy.jdk;

import org.junit.Test;

import java.lang.reflect.InvocationHandler;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/2/22
 */
public class TestPersonProxy {
    @Test
    public void testJdkProxy() {
        IPerson person = new Person();
        InvocationHandler invocationHandler = new PersonInvocation(person);
        PersonProxy personProxy = new PersonProxy(person, invocationHandler);
        IPerson iPerson = (IPerson) personProxy.getPersonProxy();
        iPerson.updateInfo();
    }
}
