package com.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/2/22
 */
public class PersonInvocation implements InvocationHandler {
    Object target;

    public PersonInvocation(Object target){
        super();
        this.target = target;
    }
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("开始！！");
        method.invoke(target);
        System.out.println("结束！！");
        return null;
    }
}
