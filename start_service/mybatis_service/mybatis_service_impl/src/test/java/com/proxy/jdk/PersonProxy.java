package com.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/2/22
 */
public class PersonProxy {
    Object target;
    InvocationHandler ih;
    private IPerson iperson;

    public PersonProxy(IPerson iperson){
        this.iperson = iperson;
    }

    public PersonProxy(Object target, InvocationHandler ih) {
        this.target = target;
        this.ih = ih;
    }

    public Object getPersonProxy() {
        return Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), ih);
    }

    public void doSomeThing(){
        System.out.println("开始");
        iperson.updateInfo();
        System.out.println("结束");
    }

    public static void main(String[] args){
        IPerson person = new Person();
        PersonProxy personProxy = new PersonProxy(person);
        personProxy.doSomeThing();
    }
}
