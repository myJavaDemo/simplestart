package com.proxy.produce;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/4/22
 */
public interface ProduceFactory {
    String creatProduce(String factoryName);
}
