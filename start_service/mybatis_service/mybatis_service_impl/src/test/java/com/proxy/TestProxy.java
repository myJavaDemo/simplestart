package com.proxy;

import org.junit.Test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

/**
 * @author : liyang18
 * @description :
 * @date : 2019/2/21
 */
public class TestProxy {
    @Test
    public void test1() {
        Person person = new Student("我");

        Person monitor = new StudentProxy(person);

        monitor.giveMoney();
    }

    @Test
    public void test2() {
        Person stu = new Student("张三");

        InvocationHandler invocationHandler = new StuInvocationHandler<>(stu);

        Person stuProxy = (Person) Proxy.newProxyInstance(Person.class.getClassLoader(),
                new Class[]{Person.class}, invocationHandler);
        stuProxy.giveMoney();
    }
}
