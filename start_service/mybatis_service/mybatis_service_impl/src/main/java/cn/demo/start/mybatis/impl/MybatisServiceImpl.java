package cn.demo.start.mybatis.impl;

import cn.demo.start.mybatis.MybatisService;
import cn.demo.start.mybatis.bean.TbUser;
import cn.demo.start.mybatis.bean.User;
import cn.demo.start.mybatis.dao.TbUserMapper;
import cn.demo.start.mybatis.dao.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/8/6
 */
@Service
public class MybatisServiceImpl implements MybatisService
{
    @Autowired(required = false)
    private UserDao userDao;

    @Autowired(required = false)
    private TbUserMapper userMapper;

    private final static Logger logger = LoggerFactory.getLogger(MybatisServiceImpl.class);

    @Override
    public String findNameById()
    {
        try
        {
            logger.info("============开始");
            User userInfo = userDao.findUserInfoById("7");
            TbUser tbUser = new TbUser();
            tbUser.setId(7L);
            TbUser user = userMapper.queryTbUserLimit1(tbUser);
            System.out.println(user);
        }catch (Exception e)
        {
            logger.info("=========错误");
            e.printStackTrace();
        }
        return "ok";
    }
}
