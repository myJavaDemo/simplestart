package cn.demo.start.mybatis.dao.base;

import cn.demo.start.mybatis.bean.TbUser;

import java.util.List;

/**
*  @author liyang
*/
public interface TbUserBaseMapper {

    int insertTbUser(TbUser object);

    int updateTbUser(TbUser object);

    int update(TbUser.UpdateBuilder object);

    List<TbUser> queryTbUser(TbUser object);

    TbUser queryTbUserLimit1(TbUser object);

}