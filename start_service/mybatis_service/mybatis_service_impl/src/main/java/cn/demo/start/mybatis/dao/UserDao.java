package cn.demo.start.mybatis.dao;

import cn.demo.start.mybatis.bean.User;
import org.apache.ibatis.annotations.Param;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/8/6
 */
public interface UserDao
{
    User findUserInfoById(@Param("id") String id);
}
