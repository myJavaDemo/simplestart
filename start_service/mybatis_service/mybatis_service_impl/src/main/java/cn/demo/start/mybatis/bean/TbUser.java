package cn.demo.start.mybatis.bean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author liyang
*/
public class TbUser implements Serializable {

    private static final long serialVersionUID = 1533540195621L;


    /**
    * 主键
    * 
    * isNullAble:0
    */
    private Long id;

    /**
    * 用户名
    * isNullAble:0
    */
    private String username;

    /**
    * 密码，加密存储
    * isNullAble:0
    */
    private String password;

    /**
    * 注册手机号
    * isNullAble:1
    */
    private String phone;

    /**
    * 注册邮箱
    * isNullAble:1
    */
    private String email;

    /**
    * 
    * isNullAble:0
    */
    private java.time.LocalDateTime created;

    /**
    * 
    * isNullAble:0
    */
    private java.time.LocalDateTime updated;


    public void setId(Long id){this.id = id;}

    public Long getId(){return this.id;}

    public void setUsername(String username){this.username = username;}

    public String getUsername(){return this.username;}

    public void setPassword(String password){this.password = password;}

    public String getPassword(){return this.password;}

    public void setPhone(String phone){this.phone = phone;}

    public String getPhone(){return this.phone;}

    public void setEmail(String email){this.email = email;}

    public String getEmail(){return this.email;}

    public void setCreated(java.time.LocalDateTime created){this.created = created;}

    public java.time.LocalDateTime getCreated(){return this.created;}

    public void setUpdated(java.time.LocalDateTime updated){this.updated = updated;}

    public java.time.LocalDateTime getUpdated(){return this.updated;}
    @Override
    public String toString() {
        return "TbUser{" +
                "id='" + id + '\'' +
                "username='" + username + '\'' +
                "password='" + password + '\'' +
                "phone='" + phone + '\'' +
                "email='" + email + '\'' +
                "created='" + created + '\'' +
                "updated='" + updated + '\'' +
            '}';
    }

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private TbUser set;

        private ConditionBuilder where;

        public UpdateBuilder set(TbUser set){
            this.set = set;
            return this;
        }

        public TbUser getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends TbUser{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<Long> idList;

        public List<Long> getIdList(){return this.idList;}

        private Long idSt;

        private Long idEd;

        public Long getIdSt(){return this.idSt;}

        public Long getIdEd(){return this.idEd;}

        private List<String> usernameList;

        public List<String> getUsernameList(){return this.usernameList;}


        private List<String> fuzzyUsername;

        public List<String> getFuzzyUsername(){return this.fuzzyUsername;}

        private List<String> rightFuzzyUsername;

        public List<String> getRightFuzzyUsername(){return this.rightFuzzyUsername;}
        private List<String> passwordList;

        public List<String> getPasswordList(){return this.passwordList;}


        private List<String> fuzzyPassword;

        public List<String> getFuzzyPassword(){return this.fuzzyPassword;}

        private List<String> rightFuzzyPassword;

        public List<String> getRightFuzzyPassword(){return this.rightFuzzyPassword;}
        private List<String> phoneList;

        public List<String> getPhoneList(){return this.phoneList;}


        private List<String> fuzzyPhone;

        public List<String> getFuzzyPhone(){return this.fuzzyPhone;}

        private List<String> rightFuzzyPhone;

        public List<String> getRightFuzzyPhone(){return this.rightFuzzyPhone;}
        private List<String> emailList;

        public List<String> getEmailList(){return this.emailList;}


        private List<String> fuzzyEmail;

        public List<String> getFuzzyEmail(){return this.fuzzyEmail;}

        private List<String> rightFuzzyEmail;

        public List<String> getRightFuzzyEmail(){return this.rightFuzzyEmail;}
        private List<java.time.LocalDateTime> createdList;

        public List<java.time.LocalDateTime> getCreatedList(){return this.createdList;}

        private java.time.LocalDateTime createdSt;

        private java.time.LocalDateTime createdEd;

        public java.time.LocalDateTime getCreatedSt(){return this.createdSt;}

        public java.time.LocalDateTime getCreatedEd(){return this.createdEd;}

        private List<java.time.LocalDateTime> updatedList;

        public List<java.time.LocalDateTime> getUpdatedList(){return this.updatedList;}

        private java.time.LocalDateTime updatedSt;

        private java.time.LocalDateTime updatedEd;

        public java.time.LocalDateTime getUpdatedSt(){return this.updatedSt;}

        public java.time.LocalDateTime getUpdatedEd(){return this.updatedEd;}

        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder idBetWeen(Long idSt,Long idEd){
            this.idSt = idSt;
            this.idEd = idEd;
            return this;
        }

        public QueryBuilder idGreaterEqThan(Long idSt){
            this.idSt = idSt;
            return this;
        }
        public QueryBuilder idLessEqThan(Long idEd){
            this.idEd = idEd;
            return this;
        }


        public QueryBuilder id(Long id){
            setId(id);
            return this;
        }

        public QueryBuilder idList(Long ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public QueryBuilder idList(List<Long> id){
            this.idList = id;
            return this;
        }

        public QueryBuilder fetchId(){
            setFetchFields("fetchFields","id");
            return this;
        }

        public QueryBuilder excludeId(){
            setFetchFields("excludeFields","id");
            return this;
        }

        public QueryBuilder fuzzyUsername (List<String> fuzzyUsername){
            this.fuzzyUsername = fuzzyUsername;
            return this;
        }

        public QueryBuilder fuzzyUsername (String ... fuzzyUsername){
            this.fuzzyUsername = solveNullList(fuzzyUsername);
            return this;
        }

        public QueryBuilder rightFuzzyUsername (List<String> rightFuzzyUsername){
            this.rightFuzzyUsername = rightFuzzyUsername;
            return this;
        }

        public QueryBuilder rightFuzzyUsername (String ... rightFuzzyUsername){
            this.rightFuzzyUsername = solveNullList(rightFuzzyUsername);
            return this;
        }

        public QueryBuilder username(String username){
            setUsername(username);
            return this;
        }

        public QueryBuilder usernameList(String ... username){
            this.usernameList = solveNullList(username);
            return this;
        }

        public QueryBuilder usernameList(List<String> username){
            this.usernameList = username;
            return this;
        }

        public QueryBuilder fetchUsername(){
            setFetchFields("fetchFields","username");
            return this;
        }

        public QueryBuilder excludeUsername(){
            setFetchFields("excludeFields","username");
            return this;
        }

        public QueryBuilder fuzzyPassword (List<String> fuzzyPassword){
            this.fuzzyPassword = fuzzyPassword;
            return this;
        }

        public QueryBuilder fuzzyPassword (String ... fuzzyPassword){
            this.fuzzyPassword = solveNullList(fuzzyPassword);
            return this;
        }

        public QueryBuilder rightFuzzyPassword (List<String> rightFuzzyPassword){
            this.rightFuzzyPassword = rightFuzzyPassword;
            return this;
        }

        public QueryBuilder rightFuzzyPassword (String ... rightFuzzyPassword){
            this.rightFuzzyPassword = solveNullList(rightFuzzyPassword);
            return this;
        }

        public QueryBuilder password(String password){
            setPassword(password);
            return this;
        }

        public QueryBuilder passwordList(String ... password){
            this.passwordList = solveNullList(password);
            return this;
        }

        public QueryBuilder passwordList(List<String> password){
            this.passwordList = password;
            return this;
        }

        public QueryBuilder fetchPassword(){
            setFetchFields("fetchFields","password");
            return this;
        }

        public QueryBuilder excludePassword(){
            setFetchFields("excludeFields","password");
            return this;
        }

        public QueryBuilder fuzzyPhone (List<String> fuzzyPhone){
            this.fuzzyPhone = fuzzyPhone;
            return this;
        }

        public QueryBuilder fuzzyPhone (String ... fuzzyPhone){
            this.fuzzyPhone = solveNullList(fuzzyPhone);
            return this;
        }

        public QueryBuilder rightFuzzyPhone (List<String> rightFuzzyPhone){
            this.rightFuzzyPhone = rightFuzzyPhone;
            return this;
        }

        public QueryBuilder rightFuzzyPhone (String ... rightFuzzyPhone){
            this.rightFuzzyPhone = solveNullList(rightFuzzyPhone);
            return this;
        }

        public QueryBuilder phone(String phone){
            setPhone(phone);
            return this;
        }

        public QueryBuilder phoneList(String ... phone){
            this.phoneList = solveNullList(phone);
            return this;
        }

        public QueryBuilder phoneList(List<String> phone){
            this.phoneList = phone;
            return this;
        }

        public QueryBuilder fetchPhone(){
            setFetchFields("fetchFields","phone");
            return this;
        }

        public QueryBuilder excludePhone(){
            setFetchFields("excludeFields","phone");
            return this;
        }

        public QueryBuilder fuzzyEmail (List<String> fuzzyEmail){
            this.fuzzyEmail = fuzzyEmail;
            return this;
        }

        public QueryBuilder fuzzyEmail (String ... fuzzyEmail){
            this.fuzzyEmail = solveNullList(fuzzyEmail);
            return this;
        }

        public QueryBuilder rightFuzzyEmail (List<String> rightFuzzyEmail){
            this.rightFuzzyEmail = rightFuzzyEmail;
            return this;
        }

        public QueryBuilder rightFuzzyEmail (String ... rightFuzzyEmail){
            this.rightFuzzyEmail = solveNullList(rightFuzzyEmail);
            return this;
        }

        public QueryBuilder email(String email){
            setEmail(email);
            return this;
        }

        public QueryBuilder emailList(String ... email){
            this.emailList = solveNullList(email);
            return this;
        }

        public QueryBuilder emailList(List<String> email){
            this.emailList = email;
            return this;
        }

        public QueryBuilder fetchEmail(){
            setFetchFields("fetchFields","email");
            return this;
        }

        public QueryBuilder excludeEmail(){
            setFetchFields("excludeFields","email");
            return this;
        }

        public QueryBuilder createdBetWeen(java.time.LocalDateTime createdSt,java.time.LocalDateTime createdEd){
            this.createdSt = createdSt;
            this.createdEd = createdEd;
            return this;
        }

        public QueryBuilder createdGreaterEqThan(java.time.LocalDateTime createdSt){
            this.createdSt = createdSt;
            return this;
        }
        public QueryBuilder createdLessEqThan(java.time.LocalDateTime createdEd){
            this.createdEd = createdEd;
            return this;
        }


        public QueryBuilder created(java.time.LocalDateTime created){
            setCreated(created);
            return this;
        }

        public QueryBuilder createdList(java.time.LocalDateTime ... created){
            this.createdList = solveNullList(created);
            return this;
        }

        public QueryBuilder createdList(List<java.time.LocalDateTime> created){
            this.createdList = created;
            return this;
        }

        public QueryBuilder fetchCreated(){
            setFetchFields("fetchFields","created");
            return this;
        }

        public QueryBuilder excludeCreated(){
            setFetchFields("excludeFields","created");
            return this;
        }

        public QueryBuilder updatedBetWeen(java.time.LocalDateTime updatedSt,java.time.LocalDateTime updatedEd){
            this.updatedSt = updatedSt;
            this.updatedEd = updatedEd;
            return this;
        }

        public QueryBuilder updatedGreaterEqThan(java.time.LocalDateTime updatedSt){
            this.updatedSt = updatedSt;
            return this;
        }
        public QueryBuilder updatedLessEqThan(java.time.LocalDateTime updatedEd){
            this.updatedEd = updatedEd;
            return this;
        }


        public QueryBuilder updated(java.time.LocalDateTime updated){
            setUpdated(updated);
            return this;
        }

        public QueryBuilder updatedList(java.time.LocalDateTime ... updated){
            this.updatedList = solveNullList(updated);
            return this;
        }

        public QueryBuilder updatedList(List<java.time.LocalDateTime> updated){
            this.updatedList = updated;
            return this;
        }

        public QueryBuilder fetchUpdated(){
            setFetchFields("fetchFields","updated");
            return this;
        }

        public QueryBuilder excludeUpdated(){
            setFetchFields("excludeFields","updated");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public TbUser build(){return this;}
    }


    public static class ConditionBuilder{
        private List<Long> idList;

        public List<Long> getIdList(){return this.idList;}

        private Long idSt;

        private Long idEd;

        public Long getIdSt(){return this.idSt;}

        public Long getIdEd(){return this.idEd;}

        private List<String> usernameList;

        public List<String> getUsernameList(){return this.usernameList;}


        private List<String> fuzzyUsername;

        public List<String> getFuzzyUsername(){return this.fuzzyUsername;}

        private List<String> rightFuzzyUsername;

        public List<String> getRightFuzzyUsername(){return this.rightFuzzyUsername;}
        private List<String> passwordList;

        public List<String> getPasswordList(){return this.passwordList;}


        private List<String> fuzzyPassword;

        public List<String> getFuzzyPassword(){return this.fuzzyPassword;}

        private List<String> rightFuzzyPassword;

        public List<String> getRightFuzzyPassword(){return this.rightFuzzyPassword;}
        private List<String> phoneList;

        public List<String> getPhoneList(){return this.phoneList;}


        private List<String> fuzzyPhone;

        public List<String> getFuzzyPhone(){return this.fuzzyPhone;}

        private List<String> rightFuzzyPhone;

        public List<String> getRightFuzzyPhone(){return this.rightFuzzyPhone;}
        private List<String> emailList;

        public List<String> getEmailList(){return this.emailList;}


        private List<String> fuzzyEmail;

        public List<String> getFuzzyEmail(){return this.fuzzyEmail;}

        private List<String> rightFuzzyEmail;

        public List<String> getRightFuzzyEmail(){return this.rightFuzzyEmail;}
        private List<java.time.LocalDateTime> createdList;

        public List<java.time.LocalDateTime> getCreatedList(){return this.createdList;}

        private java.time.LocalDateTime createdSt;

        private java.time.LocalDateTime createdEd;

        public java.time.LocalDateTime getCreatedSt(){return this.createdSt;}

        public java.time.LocalDateTime getCreatedEd(){return this.createdEd;}

        private List<java.time.LocalDateTime> updatedList;

        public List<java.time.LocalDateTime> getUpdatedList(){return this.updatedList;}

        private java.time.LocalDateTime updatedSt;

        private java.time.LocalDateTime updatedEd;

        public java.time.LocalDateTime getUpdatedSt(){return this.updatedSt;}

        public java.time.LocalDateTime getUpdatedEd(){return this.updatedEd;}


        public ConditionBuilder idBetWeen(Long idSt,Long idEd){
            this.idSt = idSt;
            this.idEd = idEd;
            return this;
        }

        public ConditionBuilder idGreaterEqThan(Long idSt){
            this.idSt = idSt;
            return this;
        }
        public ConditionBuilder idLessEqThan(Long idEd){
            this.idEd = idEd;
            return this;
        }


        public ConditionBuilder idList(Long ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public ConditionBuilder idList(List<Long> id){
            this.idList = id;
            return this;
        }

        public ConditionBuilder fuzzyUsername (List<String> fuzzyUsername){
            this.fuzzyUsername = fuzzyUsername;
            return this;
        }

        public ConditionBuilder fuzzyUsername (String ... fuzzyUsername){
            this.fuzzyUsername = solveNullList(fuzzyUsername);
            return this;
        }

        public ConditionBuilder rightFuzzyUsername (List<String> rightFuzzyUsername){
            this.rightFuzzyUsername = rightFuzzyUsername;
            return this;
        }

        public ConditionBuilder rightFuzzyUsername (String ... rightFuzzyUsername){
            this.rightFuzzyUsername = solveNullList(rightFuzzyUsername);
            return this;
        }

        public ConditionBuilder usernameList(String ... username){
            this.usernameList = solveNullList(username);
            return this;
        }

        public ConditionBuilder usernameList(List<String> username){
            this.usernameList = username;
            return this;
        }

        public ConditionBuilder fuzzyPassword (List<String> fuzzyPassword){
            this.fuzzyPassword = fuzzyPassword;
            return this;
        }

        public ConditionBuilder fuzzyPassword (String ... fuzzyPassword){
            this.fuzzyPassword = solveNullList(fuzzyPassword);
            return this;
        }

        public ConditionBuilder rightFuzzyPassword (List<String> rightFuzzyPassword){
            this.rightFuzzyPassword = rightFuzzyPassword;
            return this;
        }

        public ConditionBuilder rightFuzzyPassword (String ... rightFuzzyPassword){
            this.rightFuzzyPassword = solveNullList(rightFuzzyPassword);
            return this;
        }

        public ConditionBuilder passwordList(String ... password){
            this.passwordList = solveNullList(password);
            return this;
        }

        public ConditionBuilder passwordList(List<String> password){
            this.passwordList = password;
            return this;
        }

        public ConditionBuilder fuzzyPhone (List<String> fuzzyPhone){
            this.fuzzyPhone = fuzzyPhone;
            return this;
        }

        public ConditionBuilder fuzzyPhone (String ... fuzzyPhone){
            this.fuzzyPhone = solveNullList(fuzzyPhone);
            return this;
        }

        public ConditionBuilder rightFuzzyPhone (List<String> rightFuzzyPhone){
            this.rightFuzzyPhone = rightFuzzyPhone;
            return this;
        }

        public ConditionBuilder rightFuzzyPhone (String ... rightFuzzyPhone){
            this.rightFuzzyPhone = solveNullList(rightFuzzyPhone);
            return this;
        }

        public ConditionBuilder phoneList(String ... phone){
            this.phoneList = solveNullList(phone);
            return this;
        }

        public ConditionBuilder phoneList(List<String> phone){
            this.phoneList = phone;
            return this;
        }

        public ConditionBuilder fuzzyEmail (List<String> fuzzyEmail){
            this.fuzzyEmail = fuzzyEmail;
            return this;
        }

        public ConditionBuilder fuzzyEmail (String ... fuzzyEmail){
            this.fuzzyEmail = solveNullList(fuzzyEmail);
            return this;
        }

        public ConditionBuilder rightFuzzyEmail (List<String> rightFuzzyEmail){
            this.rightFuzzyEmail = rightFuzzyEmail;
            return this;
        }

        public ConditionBuilder rightFuzzyEmail (String ... rightFuzzyEmail){
            this.rightFuzzyEmail = solveNullList(rightFuzzyEmail);
            return this;
        }

        public ConditionBuilder emailList(String ... email){
            this.emailList = solveNullList(email);
            return this;
        }

        public ConditionBuilder emailList(List<String> email){
            this.emailList = email;
            return this;
        }

        public ConditionBuilder createdBetWeen(java.time.LocalDateTime createdSt,java.time.LocalDateTime createdEd){
            this.createdSt = createdSt;
            this.createdEd = createdEd;
            return this;
        }

        public ConditionBuilder createdGreaterEqThan(java.time.LocalDateTime createdSt){
            this.createdSt = createdSt;
            return this;
        }
        public ConditionBuilder createdLessEqThan(java.time.LocalDateTime createdEd){
            this.createdEd = createdEd;
            return this;
        }


        public ConditionBuilder createdList(java.time.LocalDateTime ... created){
            this.createdList = solveNullList(created);
            return this;
        }

        public ConditionBuilder createdList(List<java.time.LocalDateTime> created){
            this.createdList = created;
            return this;
        }

        public ConditionBuilder updatedBetWeen(java.time.LocalDateTime updatedSt,java.time.LocalDateTime updatedEd){
            this.updatedSt = updatedSt;
            this.updatedEd = updatedEd;
            return this;
        }

        public ConditionBuilder updatedGreaterEqThan(java.time.LocalDateTime updatedSt){
            this.updatedSt = updatedSt;
            return this;
        }
        public ConditionBuilder updatedLessEqThan(java.time.LocalDateTime updatedEd){
            this.updatedEd = updatedEd;
            return this;
        }


        public ConditionBuilder updatedList(java.time.LocalDateTime ... updated){
            this.updatedList = solveNullList(updated);
            return this;
        }

        public ConditionBuilder updatedList(List<java.time.LocalDateTime> updated){
            this.updatedList = updated;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private TbUser obj;

        public Builder(){
            this.obj = new TbUser();
        }

        public Builder id(Long id){
            this.obj.setId(id);
            return this;
        }
        public Builder username(String username){
            this.obj.setUsername(username);
            return this;
        }
        public Builder password(String password){
            this.obj.setPassword(password);
            return this;
        }
        public Builder phone(String phone){
            this.obj.setPhone(phone);
            return this;
        }
        public Builder email(String email){
            this.obj.setEmail(email);
            return this;
        }
        public Builder created(java.time.LocalDateTime created){
            this.obj.setCreated(created);
            return this;
        }
        public Builder updated(java.time.LocalDateTime updated){
            this.obj.setUpdated(updated);
            return this;
        }
        public TbUser build(){return obj;}
    }

}
