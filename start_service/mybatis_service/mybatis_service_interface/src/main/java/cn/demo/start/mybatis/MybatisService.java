package cn.demo.start.mybatis;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/8/6
 */
public interface MybatisService
{
    String findNameById();
}
