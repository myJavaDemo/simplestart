package cn.demo.start.mybatis;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/8/7
 */
public interface MybatisMutilSource
{
    String findTableInfo();
}
