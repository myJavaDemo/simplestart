package cn.demo.start.multi.datasorce.bean;

import java.io.Serializable;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/8/7
 */
public class AreaInfo implements Serializable
{
    private String areaCode;

    private String areaName;

    private String areaFrame;

    private String parentAreaCode;

    private String areaLevel;

    public String getAreaCode()
    {
        return areaCode;
    }

    public void setAreaCode(String areaCode)
    {
        this.areaCode = areaCode;
    }

    public String getAreaName()
    {
        return areaName;
    }

    public void setAreaName(String areaName)
    {
        this.areaName = areaName;
    }

    public String getAreaFrame()
    {
        return areaFrame;
    }

    public void setAreaFrame(String areaFrame)
    {
        this.areaFrame = areaFrame;
    }

    public String getParentAreaCode()
    {
        return parentAreaCode;
    }

    public void setParentAreaCode(String parentAreaCode)
    {
        this.parentAreaCode = parentAreaCode;
    }

    public String getAreaLevel()
    {
        return areaLevel;
    }

    public void setAreaLevel(String areaLevel)
    {
        this.areaLevel = areaLevel;
    }

    @Override
    public String toString()
    {
        return "AreaInfo{" + "areaCode='" + areaCode + '\'' + ", areaName='" + areaName + '\'' + ", areaFrame='" + areaFrame + '\'' + ", parentAreaCode='" + parentAreaCode + '\'' + ", areaLevel='" + areaLevel + '\'' + '}';
    }
}
