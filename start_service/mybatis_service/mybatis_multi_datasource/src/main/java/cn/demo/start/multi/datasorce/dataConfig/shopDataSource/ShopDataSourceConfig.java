package cn.demo.start.multi.datasorce.dataConfig.shopDataSource;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.io.IOException;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/8/7
 */
@Configuration
@MapperScan(basePackages = ShopDataSourceConfig.DAO_PACAKGE, sqlSessionFactoryRef = "shopSqlSessionFactory")
public class ShopDataSourceConfig
{
    /**
     * dao包
     */
    static final String DAO_PACAKGE = "cn.demo.start.multi.datasorce.dao.shopDao";
    /**
     * 映射xml
     */
    static final String MAPPER_LOCATION = "classpath:mapper/shopMapper/*.xml";

    static final String MYBATIS_CONFIG = "classpath:mapper/mybatis-config.xml";

    @Value("${shopInfo.datasource.url}")
    String url;

    @Value("${shopInfo.datasource.username}")
    String username;

    @Value("${shopInfo.datasource.password}")
    String password;

    @Value("${shopInfo.datasource.driver-class-name}")
    String driveClassName;

    @Bean(name = "shopDataSource")
    @Primary
    public DataSource loadShopDataSource()
    {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setDriverClassName(driveClassName);
        return dataSource;
    }

    @Bean(name = "shopSourceTransactionManager")
    @Primary
    public DataSourceTransactionManager shopSourceTransactionManager()
    {
        return new DataSourceTransactionManager(loadShopDataSource());
    }

    @Bean(name = "shopSqlSessionFactory")
    @Primary
    public SqlSessionFactory shopSqlSessionFactory(@Qualifier("shopDataSource") DataSource shopDataSource)
            throws Exception
    {
        final SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(shopDataSource);
        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(MAPPER_LOCATION));
        sqlSessionFactoryBean.setConfigLocation(new PathMatchingResourcePatternResolver()
                .getResource(MYBATIS_CONFIG));
        return sqlSessionFactoryBean.getObject();
    }
}
