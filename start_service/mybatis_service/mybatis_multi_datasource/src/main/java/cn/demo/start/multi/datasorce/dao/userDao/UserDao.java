package cn.demo.start.multi.datasorce.dao.userDao;

import cn.demo.start.multi.datasorce.bean.User;
import org.apache.ibatis.annotations.Param;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/8/7
 */
public interface UserDao
{
    User findUserInfoById(@Param("id") String id);
}
