package cn.demo.start.multi.datasorce.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.velocity.VelocityAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/8/6
 */
@Configuration
@EnableAutoConfiguration(exclude = {VelocityAutoConfiguration.class, ErrorMvcAutoConfiguration.class})
/**
 * 扫描 @Controller、@Service 注解；
 */
@ComponentScan({"com","cn"})
@MapperScan("cn.demo.start.multi.datasorce.dao")
public class WebXml extends SpringBootServletInitializer
{
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(WebXml.class);
    }
}
