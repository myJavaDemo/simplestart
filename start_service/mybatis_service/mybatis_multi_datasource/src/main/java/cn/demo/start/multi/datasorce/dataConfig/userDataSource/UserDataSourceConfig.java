package cn.demo.start.multi.datasorce.dataConfig.userDataSource;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.io.IOException;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/8/7
 */
@Configuration
@MapperScan(basePackages = UserDataSourceConfig.DAO_PACAKGE, sqlSessionFactoryRef = "userSqlSessionFactory")
public class UserDataSourceConfig
{
    /**
     * dao包
     */
    static final String DAO_PACAKGE = "cn.demo.start.multi.datasorce.dao.userDao";
    /**
     * 映射xml
     */
    static final String MAPPER_LOCATION = "classpath:mapper/userMapper/*.xml";

    @Value("${userInfo.datasource.url}")
    String url;

    @Value("${userInfo.datasource.username}")
    String username;

    @Value("${userInfo.datasource.password}")
    String password;

    @Value("${userInfo.datasource.driver-class-name}")
    String driveClassName;

    @Bean(name = "userDataSource")
    public DataSource loadUserDataSource()
    {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setDriverClassName(driveClassName);
        return dataSource;
    }

    @Bean(name = "userSourceTransactionManager")
    public DataSourceTransactionManager userSourceTransactionManager()
    {
        return new DataSourceTransactionManager(loadUserDataSource());
    }

    @Bean(name = "userSqlSessionFactory")
    public SqlSessionFactory userSqlSessionFactory(@Qualifier("userDataSource") DataSource userDataSource)
            throws Exception
    {
        final SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(userDataSource);
        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver()
                .getResources(MAPPER_LOCATION));
        return sqlSessionFactoryBean.getObject();
    }
}
