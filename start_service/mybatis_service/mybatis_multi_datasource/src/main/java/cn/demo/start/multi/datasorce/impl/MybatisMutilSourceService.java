package cn.demo.start.multi.datasorce.impl;

import cn.demo.start.multi.datasorce.bean.AreaInfo;
import cn.demo.start.multi.datasorce.bean.User;
import cn.demo.start.multi.datasorce.dao.shopDao.AreaInfoDao;
import cn.demo.start.multi.datasorce.dao.userDao.UserDao;
import cn.demo.start.mybatis.MybatisMutilSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/8/7
 */
@Service
public class MybatisMutilSourceService implements MybatisMutilSource
{
    @Autowired(required = false)
    private AreaInfoDao areaInfoDao;

    @Autowired(required = false)
    private UserDao userDao;

    @Override
    public String findTableInfo()
    {
        try
        {
            Assert.notNull(areaInfoDao, "No SqlSessionFactory specified");
            AreaInfo info = areaInfoDao.findInfo("00");
            User userInfoById = userDao.findUserInfoById("7");
            System.out.println(info.toString());
            System.out.println(userInfoById.toString());
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return "ok";
    }
}
