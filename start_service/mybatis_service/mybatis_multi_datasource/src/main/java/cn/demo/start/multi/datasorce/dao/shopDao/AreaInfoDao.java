package cn.demo.start.multi.datasorce.dao.shopDao;

import cn.demo.start.multi.datasorce.bean.AreaInfo;
import org.apache.ibatis.annotations.Param;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/8/7
 */
public interface AreaInfoDao
{
    AreaInfo findInfo(@Param("areaCode") String areaCode);
}
