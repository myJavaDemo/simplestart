package cn.demo.start.habernate;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/7/31
 */
public interface UserService
{
    String findOne();
}
