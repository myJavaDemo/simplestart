package cn.demo.start.habernate.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.velocity.VelocityAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ErrorMvcAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

@Configuration
@EnableAutoConfiguration(exclude = {VelocityAutoConfiguration.class, ErrorMvcAutoConfiguration.class})
/**
 * 扫描 @Controller、@Service 注解；
 * 扫描 @Repository 注解；
 * 扫描 @Entity 注解；
 */
@ComponentScan({"cn", "com"})
@EnableJpaRepositories({"cn", "com"})
@EntityScan({"cn", "com"})
public class WebXml extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(WebXml.class);
	}
}
