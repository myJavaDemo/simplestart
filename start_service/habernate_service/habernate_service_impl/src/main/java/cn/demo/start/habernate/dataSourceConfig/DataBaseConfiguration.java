package cn.demo.start.habernate.dataSourceConfig;

import com.alibaba.druid.pool.DruidDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/7/31
 */
@Configuration
@EnableTransactionManagement
public class DataBaseConfiguration implements EnvironmentAware
{
    private RelaxedPropertyResolver resolver;

    private Logger logger = LoggerFactory.getLogger(DataBaseConfiguration.class);

    @Override
    public void setEnvironment(Environment environment)
    {
        resolver = new RelaxedPropertyResolver(environment, "spring.datasource.");
    }

    @Bean
    public DataSource writeDataSource()
    {
        logger.error("=============注入datasource");
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(resolver.getProperty("url"));
        dataSource.setUsername(resolver.getProperty("username"));
        dataSource.setPassword(resolver.getProperty("password"));
        dataSource.setDriverClassName(resolver.getProperty("driver-class-name"));
        dataSource.setInitialSize(Integer.parseInt(resolver.getProperty("initialSize")));
        dataSource.setMinIdle(Integer.valueOf(resolver.getProperty("minIdle")));
        dataSource.setMaxWait(Long.valueOf(resolver.getProperty("maxWait")));
        dataSource.setMaxActive(Integer.valueOf(resolver.getProperty("maxActive")));
        dataSource.setMinEvictableIdleTimeMillis(Long.valueOf(resolver.getProperty("minEvictableIdleTimeMillis")));
        return dataSource;
    }
}
