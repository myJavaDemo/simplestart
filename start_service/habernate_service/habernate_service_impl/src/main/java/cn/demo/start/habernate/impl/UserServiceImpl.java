package cn.demo.start.habernate.impl;

import cn.demo.start.habernate.UserService;
import cn.demo.start.habernate.dao.HibernateBaseDao;
import cn.demo.start.habernate.dao.IHibernateBaseDao;
import cn.demo.start.habernate.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/7/31
 */
@Service
public class UserServiceImpl implements UserService
{
    @Override
    public String findOne()
    {
        Map<String, Object> map = new HashMap<>(16);
        try
        {
            IHibernateBaseDao<User> hibernateBaseDao = new HibernateBaseDao<>();
            User user = hibernateBaseDao.get(7L);
            System.out.println(user.toString());
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return "ok";
    }
}
