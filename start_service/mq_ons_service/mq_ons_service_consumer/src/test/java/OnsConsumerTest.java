import com.aliyun.openservices.ons.api.*;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * @author : liyang18
 * @description :
 * @date : 2018/8/13
 */
public class OnsConsumerTest
{
    @Test
    public void comsumer() throws InterruptedException
    {
        Properties properties = new Properties();
        // 您在控制台创建的 Consumer ID
        properties.put(PropertyKeyConst.ConsumerId, "CID_PROD_LY_TEST");
        // AccessKey 阿里云身份验证，在阿里云服务器管理控制台创建
        properties.put(PropertyKeyConst.AccessKey, "LTAI6jjFoOklS4WE");
        // SecretKey 阿里云身份验证，在阿里云服务器管理控制台创建
        properties.put(PropertyKeyConst.SecretKey, "Ky24ZU6hX6OPpFzzC03Vz23b6R026F");
        // 设置 TCP 接入域名（此处以公共云生产环境为例）
        properties.put(PropertyKeyConst.ONSAddr,
                "http://onsaddr-internet.aliyun.com/rocketmq/nsaddr4client-internet");

        // 集群订阅方式 (默认)
        // properties.put(PropertyKeyConst.MessageModel, PropertyValueConst.CLUSTERING);
        // 广播订阅方式
        // properties.put(PropertyKeyConst.MessageModel, PropertyValueConst.BROADCASTING);
        Consumer consumer = ONSFactory.createConsumer(properties);
        consumer.subscribe("ly_test_mq_18", "*", new MessageListener() { //订阅多个 Tag
            public Action consume(Message message, ConsumeContext context) {
                System.out.println("Receive: " + message);
                try
                {
                    String msg = new String(message.getBody(), "utf-8");
                    System.out.println("body: " + msg);

                }
                catch (UnsupportedEncodingException e)
                {
                    e.printStackTrace();
                }
                return Action.CommitMessage;
            }
        });
        //订阅另外一个 Topic
        /*consumer.subscribe("TopicTestMQ-Other", "*", new MessageListener() { //订阅全部 Tag
            public Action consume(Message message, ConsumeContext context) {
                System.out.println("Receive: " + message);
                return Action.CommitMessage;
            }
        });*/
        consumer.start();
        System.out.println("Consumer Started");
        Thread.sleep(300000);
    }
}
